<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sign Up Form</title>
    </head>

    <body>
         <!-- Text for Heading -->
         <div>
            <h1>Buat Account Baru!</h1>
            <h2>Sign Up Form</h2>
        </div>

        <!-- Made a New Form for Name -->
        <form action="/simpandata" method="POST">
        @csrf
        <div>
            <label for="1nme">First name :</label> <br><br>
            <input type="text" name="first" id="1nme" placeholder="first name"> <br><br>
            <label for="2nme">Last name :</label>  <br><br>
            <input type="text" name="last" id="2nme" placeholder="last name"> <br><br>
        </div> 


        <!-- Made Radio Button for Gender -->
        <div>
            <label>Gender :</label> <br><br>
            <input type="radio" name="gender" value="L">Male <br>
            <input type="radio" name="gender" value="P">Female <br>
            <input type="radio" name="gender" value="o">Other <br><br>
        </div>

        <!-- Made an Option for Nationality -->
        <div>
            <label>Nationality :</label>    <br><br>
            <select>
                <option>Indonesian</option>
                <option>Japanese</option>
                <option>Korean</option>
                <option>Deutche</option>    
            </select>
            <br><br>
        </div>

        <!-- Made Checkbox for Language Spoken -->
        <div>
            <label>Language Spoken :</label>    <br><br>
            <input type="checkbox" name="LS" value="Indo">Bahasa Indonesia <br>
            <input type="checkbox" name="LS" value="Eng">English <br>
            <input type="checkbox" name="LS" value="Jpn">Japanese <br>
            <input type="checkbox" name="LS" value="Othr">Other <br><br>
        </div>

        <!-- Made TextArea for Bio -->
        <div>
            <label for="bio">Bio :</label>   <br><br>
            <textarea cols="40" rows="10" id="bio"></textarea>  <br>
        
        <!-- Sign Up Button -->
            <!-- <a href="/register"><input type="submit" value="Sign Up"></a> -->
            <input type="submit" value="Sign Up">
        </div>

        </form>
        
        
    </body>
</html>
