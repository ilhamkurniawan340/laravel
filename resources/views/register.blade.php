<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcome!</title>
    </head>

    <body>
        <!-- Just add Text for Heading -->
        <h1>Selamat Datang {{$first}} {{$last}}</h1>
        <h2>Terimakasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>        
    </body>
</html>