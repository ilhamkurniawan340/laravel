<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Day 2

// Route::get('/', 'HomeController@main');

// Route::get('/welcome', 'AuthController@welcome');

// Route::get('/register', 'AuthController@register')->name("register");

// Route::post('/simpandata', 'AuthController@handleregister');

// Day 3

// Route::get('/master', function()
// {
//     return view('master');
// });

// 2 route, / and /data-tables

Route::get('/', function()
{
    return view('master');
});

// Route::get('/data-tables', function()
// {
//     return view('datatables');
// });

// Day 5 CRUD 1

Route::get('/pertanyaan', 'PertanyaanController@index');

Route::get('/pertanyaan/create', 'PertanyaanController@create');

Route::post('/pertanyaan', 'PertanyaanController@store');

// Route::get('/pertanyaan/create', function()
// {
//     return view('home');
// });