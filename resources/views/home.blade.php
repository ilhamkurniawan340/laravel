<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tampilan Home, Hello!</title>
    </head>

    <body>
        <!-- add Text, H1, H2 -->
        <h1>SanberBook</h1>
        <h2>Social Media Developer Santai Berkualitas</h2>
        <label>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</label> 
        
        <h3>Benefit Join di SanberBook</h3>
        <!-- Unordered List -->
        <ul>
            <li>Mendapatkan motivasi dari sesama developer</li>
            <li>Sharing knowledge dari para mastah Sanber</li>
            <li>Dibuat oleh canon web developer terbaik</li>
        </ul>

        <h3>Cara Bergabung ke SanberBook</h3>
        <!-- Ordered List -->
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftar di <a href="/welcome">Form Sign Up</a></li>
            <li>Selesai!</li>
        </ol>
    </body>
</html>