<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    private $data;

    public function welcome()
    {
        
        return view('welcome');
    }

    public function register()
    {
        dump($this->data);
        return view('register')->with($this->data);
    }

    public function handleregister(Request $request)
    {
        $this->data = [
            "first" => $request->first,
            "last" => $request->last
        ]; 

        return view('register')->with($this->data);
    }
}

