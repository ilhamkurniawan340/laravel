<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //
    public function index()
    {
        return view('master');
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
       // dd($request->all());
       
       $query = DB::table('pertanyaan')->insert(
           [
               "judul" => $request["title"],
               "isi" => $request["body"]
           ]
           );

           return redirect('/pertanyaan/create');
    }
}
